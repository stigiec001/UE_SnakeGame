// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Food.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	Spawn();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawner::Spawn()
{
	for (int i = 0; i < count; ++i)
	{
		float x = FMath::RandRange(-13, 13);
		float y = FMath::RandRange(-13, 13);
		FVector NewVector(x * 15, y * 15, 0);
		FTransform NewTransform(NewVector);

		AFood* SpawnedObject = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);

		SpawnedObject->Spawner = this;
	}
}

