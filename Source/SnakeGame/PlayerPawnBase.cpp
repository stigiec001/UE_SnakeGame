// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>("Camera");
	PawnCamera->ProjectionMode = ECameraProjectionMode::Orthographic;
	PawnCamera->SetOrthoWidth(800);
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(270, 0, 0));
	SetActorLocation(FVector(0, 0, 900));

	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->PlayerPawn = this;
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		auto* LastMovementDirection = &SnakeActor->LastMovementDirection;
		if (value > 0 && *LastMovementDirection != EMovementDirection::DOWN)
		{
			SnakeActor->MovementDirections.Add(EMovementDirection::UP);
			//*LastMovementDirection = EMovementDirection::UP;

		}
		else if (value < 0 && *LastMovementDirection != EMovementDirection::UP)
			SnakeActor->MovementDirections.Add(EMovementDirection::DOWN);
			//*LastMovementDirection = EMovementDirection::DOWN;
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		auto* LastMovementDirection = &SnakeActor->LastMovementDirection;
		if (value > 0 && *LastMovementDirection != EMovementDirection::LEFT)
		{
			SnakeActor->MovementDirections.Add(EMovementDirection::RIGHT);
			//*LastMovementDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && *LastMovementDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->MovementDirections.Add(EMovementDirection::LEFT);
			//*LastMovementDirection = EMovementDirection::LEFT;
		}
	}
}

