// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Components/InputComponent.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 15.f;
	MovementSpeed = 15.f;
	MovementDirections.Add(EMovementDirection::DOWN);

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
	FVector NewLocation(SnakeElements.Num() * ElementSize , 0, 0);
	FTransform NewTransform(NewLocation);
	ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	NewSnakeElement->Snake = this;
	NewSnakeElement->MeshComponent->SetVisibility(false);
	if (!SnakeElements.Add(NewSnakeElement))
		NewSnakeElement->SetFirstElement();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(0, 0, 0);
	float MovementSpeedDelta = ElementSize;

	//if (MovementDirections.IsEmpty())
	//{
	//	LastMovementDirection = MovementDirections[0];
	//	MovementDirections.HeapRemoveAt(0);
	//}

	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;							 
	case EMovementDirection::DOWN:		 
		MovementVector.X -= MovementSpeedDelta;
		break;							 
	case EMovementDirection::LEFT:		 
		MovementVector.Y += MovementSpeedDelta;
		break;							 
	case EMovementDirection::RIGHT:		 
		MovementVector.Y -= MovementSpeedDelta;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	FVector Location = SnakeElements[0]->GetActorLocation();
	if (Location.X > 200 || Location.X < -200)
	{
		Location.X *= -0.925;
		SnakeElements[0]->SetActorLocation(Location);
	}
	if (Location.Y > 200 || Location.Y < -200)
	{
		Location.Y *= -0.925;
		SnakeElements[0]->SetActorLocation(Location);
	}
	SnakeElements[0]->MeshComponent->SetVisibility(true);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

